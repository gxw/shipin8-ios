//
//  GuidesViewController.m
//  shipin8-ios
//
//  Created by gxw on 14-1-22.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "GuidesViewController.h"

@interface GuidesViewController ()
@property (strong, nonatomic) UIPageControl *pageControl;
@property (strong, nonatomic) UIScrollView *scrollview;
@end

@implementation GuidesViewController

- (void)loadView
{
    [super loadView];
    
    UIScrollView *view = [[UIScrollView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _scrollview = view;
    
    [view setUserInteractionEnabled:YES];
    [view setScrollEnabled:YES];
    
    //NO 发送滚动的通知 但是就算手指移动 scroll也不会动了 YES 发送通知 scroo可以移动
    [view setCanCancelContentTouches:YES];
    [view setBounces:NO];
    [view setPagingEnabled:YES];
    // NO 立即通知touchesShouldBegin:withEvent:inContentView 看是否滚动 scroll
    [view setDelaysContentTouches:NO];
    view.delegate = self;
    [view setContentSize:CGSizeMake(960, self.view.frame.size.height)];
    
    self.view = view;
    
    UIView *contentView1 = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    contentView1.backgroundColor = [UIColor redColor];
    contentView1.alpha = 0.5;
    
    UITextView *text = [[UITextView alloc] initWithFrame:(CGRectMake(50, 150, 220, 30))];
    text.editable = NO;
    text.text = @"It's the first guide view 。。。。。。->";
    text.backgroundColor = [UIColor clearColor];
    [contentView1 addSubview:text];
    
    [self.view addSubview:contentView1];
    
    UIView *contentView2 = [[UIView alloc] initWithFrame:CGRectMake(320, 0, self.view.frame.size.width, self.view.frame.size.height)];
    contentView2.backgroundColor = [UIColor grayColor];
    contentView2.alpha = 0.5;
    
    UITextView *text2 = [[UITextView alloc] initWithFrame:(CGRectMake(50, 150, 220, 30))];
    text2.editable = NO;
    text2.text = @"It's the second guide view 。。。。->";
    text2.backgroundColor = [UIColor clearColor];
    [contentView2 addSubview:text2];
    [self.view addSubview:contentView2];
    
    UIView *contentView3 = [[UIView alloc] initWithFrame:CGRectMake(640, 0, self.view.frame.size.width, self.view.frame.size.height)];
    contentView3.backgroundColor = [UIColor blueColor];
    contentView3.alpha = 0.5;
    
    UITextView *text3 = [[UITextView alloc] initWithFrame:(CGRectMake(50, 150, 220, 30))];
    text3.editable = NO;
    text3.text = @"It's the last guide view 。。。。。。->";
    text3.backgroundColor = [UIColor clearColor];
    [contentView3 addSubview:text3];
    
    UIButton *button = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    //给定button在view上的位置
    
    button.frame = CGRectMake(110, 300, 100, 25);
    [button setTitle:@"开始体验" forState: UIControlStateNormal];
    
    
    //button背景色
    CALayer *layer = button.layer;
    layer.backgroundColor = [[UIColor clearColor] CGColor];
    layer.borderColor = [[UIColor darkGrayColor] CGColor];
    layer.cornerRadius = 4.0f;
    layer.borderWidth = 0.5f;
    
    
    [button addTarget:self action:@selector(removeGuideView) forControlEvents:UIControlEventTouchDown];
    [contentView3 addSubview:button];
    
    [self.view addSubview:contentView3];
    
    CGRect framePageControl = CGRectMake(40, 450, 250, 30);
    _pageControl = [[UIPageControl alloc] initWithFrame:framePageControl];
    _pageControl.hidesForSinglePage = YES;
    _pageControl.userInteractionEnabled = NO;
    _pageControl.backgroundColor = [UIColor clearColor];
    _pageControl.numberOfPages = 3;
    
    [self.view addSubview:_pageControl];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 滚动停止时，触发该函数
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSLog(@"scrollViewDidEndDecelerating  -   End of Scrolling.");
    int page = _scrollview.contentOffset.x / 320; //通过滚动的偏移量来判断目前页面所对应的小白点
    _pageControl.currentPage = page; //pagecontroll响应值的变化
    _pageControl.frame = CGRectMake(40 + 320*page, 450, _pageControl.frame.size.width, _pageControl.frame.size.height);
}

- (void)removeGuideView
{
    [self.view removeFromSuperview];
    [self.delegate showMainView];
}

@end
