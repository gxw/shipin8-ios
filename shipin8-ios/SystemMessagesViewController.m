//
//  SystemMessagesViewController.m
//  shipin8-ios
//
//  Created by gxw on 14-1-24.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "SystemMessagesViewController.h"
#import "SystemMsgCell.h"
#import "MJRefresh.h"

@interface SystemMessagesViewController ()<MJRefreshBaseViewDelegate>
{
    MJRefreshHeaderView *_header;
    MJRefreshFooterView *_footer;
}

@end

@implementation SystemMessagesViewController

- (void)loadView
{
    [super loadView];
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    self.view.backgroundColor = [UIColor greenColor];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createTabView];
    
    // 3.集成刷新控件
    // 3.1.下拉刷新
    [self addHeader];
    
    // 3.2.上拉加载更多
    [self addFooter];
}

- (void)createTabView
{
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, 320, [UIScreen mainScreen].applicationFrame.size.height - 90) style:UITableViewStyleGrouped];
    tableView.backgroundColor = [UIColor clearColor];
    [tableView setDelegate:self];
    [tableView setDataSource:self];
    tableView.scrollEnabled = YES;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[SystemMsgCell class] forCellReuseIdentifier:@"Cell"];
    self.tableView = tableView;
    
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"default"]];
    [tempImageView setFrame:self.tableView.frame];
    self.tableView.backgroundView = tempImageView;
    
    tableView.separatorStyle = NO;
}


- (void)addFooter
{
    MJRefreshFooterView *footer = [MJRefreshFooterView footer];
    footer.scrollView = self.tableView;
    footer.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        // 增加5条假数据
//        for (int i = 0; i<5; i++) {
//            int random = arc4random_uniform(1000000);
//            [_fakeData addObject:[NSString stringWithFormat:@"随机数据---%d", random]];
//        }
//        
//        // 模拟延迟加载数据，因此2秒后才调用）
//        // 这里的refreshView其实就是footer
        [self performSelector:@selector(doneWithView:) withObject:refreshView afterDelay:2.0];
        
        NSLog(@"%@----开始进入刷新状态", refreshView.class);
    };
    _footer = footer;
}

- (void)addHeader
{
    MJRefreshHeaderView *header = [MJRefreshHeaderView header];
    header.scrollView = self.tableView;
    header.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        // 进入刷新状态就会回调这个Block
        
        // 增加5条假数据
//        for (int i = 0; i<5; i++) {
//            int random = arc4random_uniform(1000000);
//            [_fakeData insertObject:[NSString stringWithFormat:@"随机数据---%d", random] atIndex:0];
//        }
//        
//        // 模拟延迟加载数据，因此2秒后才调用）
//        // 这里的refreshView其实就是header
        [self performSelector:@selector(doneWithView:) withObject:refreshView afterDelay:2.0];
        
        NSLog(@"%@----开始进入刷新状态", refreshView.class);
    };
    header.endStateChangeBlock = ^(MJRefreshBaseView *refreshView) {
        // 刷新完毕就会回调这个Block
        NSLog(@"%@----刷新完毕", refreshView.class);
    };
    header.refreshStateChangeBlock = ^(MJRefreshBaseView *refreshView, MJRefreshState state) {
        // 控件的刷新状态切换了就会调用这个block
        switch (state) {
            case MJRefreshStateNormal:
                NSLog(@"%@----切换到：普通状态", refreshView.class);
                break;
                
            case MJRefreshStatePulling:
                NSLog(@"%@----切换到：松开即可刷新的状态", refreshView.class);
                break;
                
            case MJRefreshStateRefreshing:
                NSLog(@"%@----切换到：正在刷新状态", refreshView.class);
                break;
            default:
                break;
        }
    };
    [header beginRefreshing];
    _header = header;
}

//指定有多少个分区(Section)，默认为1
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 5;
}

//指定每个分区中有多少行，默认为1
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

//绘制Cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    SystemMsgCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SystemMsgCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *title = @"高度不变获取宽度，获取字符串不折行单行显示时所需要的长度";
    CGSize size1 = [title sizeWithFont:cell.title.font constrainedToSize:CGSizeMake(cell.title.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    //根据计算结果重新设置UILabel的尺寸
    [cell.title setFrame:CGRectMake(cell.title.frame.origin.x, cell.title.frame.origin.y, size1.width, size1.height)];
    cell.title.text = title;
    
    NSString *content1 = @"NOTE 1: If you have any local commits or changes to the Specs repository which are not merged, you should ensure you have a copy of them. I would recommend that you manually copy these changes over and re-commit them. You can fix your repository without deleting, however, this is not a simple process, so we are instead recommending that you delete your copy of the Specs repository and any forks of it.";
    NSString *content2 = @"NOTE 1: If you have any local commits or changes to the Specs repository which are not merged.";
    NSMutableString *content = [[NSMutableString alloc] init];
    if (indexPath.section %2 != 0) {
        [content appendString:content1];
    } else {
        [content appendString:content2];
    }
    
    CGSize size2 = [content sizeWithFont:cell.content.font constrainedToSize:CGSizeMake(cell.content.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    //根据计算结果重新设置UILabel的尺寸
    [cell.content setFrame:CGRectMake(cell.content.frame.origin.x, cell.title.frame.size.height + 10, size2.width, size2.height)];
    cell.content.text = content;
    
    [cell setFrame:CGRectMake(cell.frame.origin.x -30 , cell.frame.origin.y, cell.frame.size.width - 60, size1.height + size2.height + 10)];
    
    if (indexPath.row == 0) {
        [cell setFrame:CGRectMake(cell.frame.origin.x -30 , cell.frame.origin.y + 10, cell.frame.size.width - 60, size1.height + size2.height + 10)];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    SystemMsgCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SystemMsgCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *title = @"高度不变获取宽度，获取字符串不折行单行显示时所需要的长度";
    CGSize size1 = [title sizeWithFont:cell.title.font constrainedToSize:CGSizeMake(cell.title.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    
    NSString *content1 = @"NOTE 1: If you have any local commits or changes to the Specs repository which are not merged, you should ensure you have a copy of them. I would recommend that you manually copy these changes over and re-commit them. You can fix your repository without deleting, however, this is not a simple process, so we are instead recommending that you delete your copy of the Specs repository and any forks of it.";
    NSString *content2 = @"NOTE 1: If you have any local commits or changes to the Specs repository which are not merged.";
    NSMutableString *content = [[NSMutableString alloc] init];
    if (indexPath.section %2 != 0) {
        [content appendString:content1];
    } else {
        [content appendString:content2];
    }
    CGSize size2 = [content sizeWithFont:cell.content.font constrainedToSize:CGSizeMake(cell.content.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    
    
    return size1.height + size2.height + 15;
}

- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [self.tableView reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(80, -10, tableView.frame.size.width - 160, 13)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(90, 0, tableView.frame.size.width - 180, 13)];
    [label setFont:[UIFont boldSystemFontOfSize:12]];
    label.backgroundColor = [UIColor grayColor];
    NSString *string =@"2012-12-12 12:23:59";
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor clearColor]]; //your background color...
    return view;
}
/*
 // Override to support conditional editing of the table view.
 //划动cell是否出现del按钮
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */
/*
 // Override to support editing the table view.
 //编辑状态
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */
/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */
/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */
#pragma mark - Table view delegate
//选中Cell响应事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     //     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    NSLog(@"indexpath is: %@", indexPath);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_header free];
    [_footer free];
}

@end
