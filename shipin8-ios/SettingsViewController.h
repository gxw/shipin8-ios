//
//  SettingsViewController.h
//  shipin8-ios
//
//  Created by gxw on 14-2-8.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController<UICollectionViewDelegate>

@end
