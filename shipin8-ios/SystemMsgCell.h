//
//  SystemMsgCell.h
//  shipin8-ios
//
//  Created by gxw on 14-2-7.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SystemMsgCell : UITableViewCell
@property (nonatomic,retain) UILabel *title;
@property (nonatomic,retain) UILabel *content;
@end
