//
//  ScanViewController.m
//  shipin8-ios
//
//  Created by gxw on 14-2-9.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "ScanViewController.h"
#import "ZBarSDK.h"

@interface ScanViewController ()<ZBarReaderDelegate,ZBarReaderViewDelegate>
@property (nonatomic,retain) UILabel *content;
@property (nonatomic,retain) UIImageView *imageView;
@property (nonatomic,retain) ZBarReaderController *reader;
@end

@implementation ScanViewController

- (void)loadView
{
    [super loadView];
    self.title = @"二维码";
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    self.view.backgroundColor = [UIColor grayColor];
    
    self.tabBarController.tabBar.hidden = YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    ZBarReaderViewController *reader = [ZBarReaderViewController new];
//    _reader = reader;
//    
//    reader.readerDelegate = self;
//    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
//    reader.enableCache = YES;
//    reader.showsZBarControls = NO;
//    reader.view.frame = [UIScreen mainScreen].applicationFrame;
//
//    ZBarImageScanner *scanner = reader.scanner;
//    // TODO: (optional) additional reader configuration here
//    
//    // EXAMPLE: disable rarely used I2/5 to improve performance
//    [scanner setSymbology: ZBAR_I25
//                   config: ZBAR_CFG_ENABLE
//                       to: 0];
//    
//    // present and release the controller
////    [self presentModalViewController: reader animated: YES];
//    [self.view addSubview:reader.view];
    
    ZBarReaderView *reader = [ZBarReaderView new];
    reader.frame = [UIScreen mainScreen].applicationFrame;
    [self.view addSubview:reader];
    
    reader.readerDelegate = self;
    reader.tracksSymbols=YES;
    
    ZBarImageScanner *scanner = reader.scanner;
    [scanner setSymbology: ZBAR_I25
             config: ZBAR_CFG_ENABLE
             to: 0];
    
    reader.tag = 99999999;
    [reader start];
    
    _content = [[UILabel alloc] initWithFrame:CGRectMake(20, 80, 280, 40)];
    _content.font = [UIFont boldSystemFontOfSize:15.0f];  //UILabel的字体大小
    _content.numberOfLines = 0;
    _content.layer.opacity = 0.7;
    _content.text = @"alksdhfkajshdfkajsdhfkjasdf";
    [self.view addSubview:_content];
    
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 140, 280, 280)];
    _imageView.layer.opacity = 0.7;
    [self.view addSubview:_imageView];
}

- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    // EXAMPLE: do something useful with the barcode data
    _content.text = symbol.data;
    
    // EXAMPLE: do something useful with the barcode image
    _imageView.image =
    [info objectForKey: UIImagePickerControllerOriginalImage];
    
    // ADD: dismiss the controller (NB dismiss from the *reader*!)
//    [reader dismissModalViewControllerAnimated: YES];
}

-(void) readerView: (ZBarReaderView*) view
    didReadSymbols: (ZBarSymbolSet*) syms
         fromImage: (UIImage*) img
{
    
    for(ZBarSymbol *sym in syms) {
        
        _content.text = sym.data;
//        _imageView.image = img;
//        [view stop];
        
//        [self closeCameraScanner];
        
        // I am also setting reader to NIL but I don't really know if this is necessary or not.
        
//        reader=nil;
    }
    
    
}


-(void)closeCameraScanner{
    
    UIView * v = [self.view viewWithTag:99999999];
    if (nil != v) {
        [v removeFromSuperview];
    }
    
    [self.view endEditing:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
