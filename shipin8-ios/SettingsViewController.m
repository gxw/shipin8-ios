//
//  SettingsViewController.m
//  shipin8-ios
//
//  Created by gxw on 14-2-8.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "SettingsViewController.h"
#import "MJRefresh.h"
#import "VideoCellView.h"
#import "ScanViewController.h"

@interface SettingsViewController ()<MJRefreshBaseViewDelegate>
{
    MJRefreshHeaderView *_header;
    MJRefreshFooterView *_footer;
}

@property (nonatomic,retain) UILabel *titleLabel;
@property (nonatomic,retain) UIButton *upgradeButton;
@property UICollectionView *collectionView;
@end

@implementation SettingsViewController

- (void)loadView
{
    [super loadView];
    self.title = @"设备管理";
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.view.backgroundColor = [UIColor yellowColor];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(add)];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    //设置header view的大小
    flowLayout.headerReferenceSize = CGSizeMake(0, 44);
    //每个cell的大小
    [flowLayout setItemSize:CGSizeMake(280,200)];
    
    //设置方向（向下还是向右）垂直Vertical  水平Horizontal
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    //设置距离边框的高度
    flowLayout.sectionInset = UIEdgeInsetsMake(10, 0, 10, 0);//top、lef、bottom、right
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame collectionViewLayout:flowLayout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.alwaysBounceVertical = YES;
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.collectionView];
    
    [self.collectionView registerClass:[VideoCellView class] forCellWithReuseIdentifier:@"Cell"];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    [self.collectionView reloadData];
    
    self.collectionView.frame = CGRectMake(0, 0, self.collectionView.frame.size.width, self.collectionView.frame.size.height);
    
    // 3.集成刷新控件
    // 3.1.下拉刷新
    MJRefreshHeaderView *header = [MJRefreshHeaderView header];
    header.scrollView = self.collectionView;
    header.delegate = self;
    // 自动刷新
    [header beginRefreshing];
    _header = header;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return 5;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{

    if (kind == UICollectionElementKindSectionHeader) {
        
        UICollectionReusableView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
            
        if (reusableview==nil) {
            reusableview=[[UICollectionReusableView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        }
            
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, 15, 100, 25)];
        _titleLabel.font = [UIFont boldSystemFontOfSize:15.0f];  //UILabel的字体大小
        _titleLabel.numberOfLines = 0;  //必须定义这个属性，否则UILabel不会换行
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textAlignment = NSTextAlignmentLeft;  //文本对齐方式
        _titleLabel.text = @"已添加设备(2)";
        [reusableview addSubview:_titleLabel];
        
        _upgradeButton = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        _upgradeButton.frame = CGRectMake(195, 15, 100, 25);
        [_upgradeButton setTitle:@"一键升级" forState: UIControlStateNormal];
        //button背景色
        CALayer *layer = _upgradeButton.layer;
        layer.backgroundColor = [[UIColor blueColor] CGColor];
        layer.cornerRadius = 4.0f;
        _upgradeButton.titleLabel.font = [UIFont systemFontOfSize:15];
        
        [_upgradeButton addTarget:self action:@selector(upgradeMachines) forControlEvents:UIControlEventTouchDown];
        [reusableview addSubview:_upgradeButton];

        return reusableview;
    }
    return nil;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    VideoCellView *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (indexPath.row > 1) {
        cell.imageView.image = [UIImage imageNamed:@"forward2-128"];
        cell.textView.text = @"卧室摄像头";
    } else {
        cell.imageView.image = [UIImage imageNamed:@"contacts-128"];
        cell.textView.text = @"大厅摄像头";
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    VideoCellView *cell =[collectionView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor redColor];
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    VideoCellView *cell =[collectionView cellForItemAtIndexPath:indexPath];
    cell.contentView.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.contentView.backgroundColor = [UIColor underPageBackgroundColor];
}

- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
//    [self.collectionView reloadData];
    
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

#pragma mark - 刷新控件的代理方法
#pragma mark 开始进入刷新状态
- (void)refreshViewBeginRefreshing:(MJRefreshBaseView *)refreshView
{
    NSLog(@"%@----开始进入刷新状态", refreshView.class);
    // 2.2秒后刷新表格UI
    [self performSelector:@selector(doneWithView:) withObject:refreshView afterDelay:2.0];
}

#pragma mark 刷新完毕
- (void)refreshViewEndRefreshing:(MJRefreshBaseView *)refreshView
{
    NSLog(@"%@----刷新完毕", refreshView.class);
}

#pragma mark 监听刷新状态的改变
- (void)refreshView:(MJRefreshBaseView *)refreshView stateChange:(MJRefreshState)state
{
    switch (state) {
        case MJRefreshStateNormal:
            NSLog(@"%@----切换到：普通状态", refreshView.class);
            break;
            
        case MJRefreshStatePulling:
            NSLog(@"%@----切换到：松开即可刷新的状态", refreshView.class);
            break;
            
        case MJRefreshStateRefreshing:
            NSLog(@"%@----切换到：正在刷新状态", refreshView.class);
            break;
        default:
            break;
    }
}

/**
 为了保证内部不泄露，在dealloc中释放占用的内存
 */
- (void)dealloc
{
    [_header free];
    [_footer free];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)upgradeMachines
{
    NSLog(@"123");
}

- (void)add
{
    ScanViewController *scanViewController = [[ScanViewController alloc] init];
    [self.navigationController pushViewController:scanViewController animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    if (animated == YES) {
        self.tabBarController.tabBar.hidden = NO;
    }
}
@end
